const submitForm = document.querySelector('.form');
const imageURL = document.querySelector('#imgURL');
const topText = document.querySelector('#topText');
const bottomText = document.querySelector('#bottomText');
const imgContainer = document.querySelector('#img-container');


submitForm.addEventListener("submit", function (event) {
  event.preventDefault();

  const div = document.createElement("div");
  div.classList.add("single-img");

  const newImage = document.createElement('img');
  newImage.src = imageURL.value;

  const textOnTop = document.createElement("div");
  textOnTop.innerText = topText.value;
  textOnTop.classList.add("top");

  const textOnBottom = document.createElement("div");
  textOnBottom.innerText = bottomText.value;
  textOnBottom.classList.add("bottom");

  div.appendChild(newImage);
  div.appendChild(textOnTop);
  div.appendChild(textOnBottom);
  imgContainer.appendChild(div);
  submitForm.reset();

})

// event listener for deleting the div with class single-img
imgContainer.addEventListener("click", function (event) {
  if (event.target.tagName === "IMG") {
    event.target.parentElement.remove();
  }
});


